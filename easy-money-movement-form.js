class EasyMoneyMovementForm extends Polymer.Element {

  static get is() {
    return 'easy-money-movement-form';
  }

  static get properties() {
    return {
      id: {
        type: Number
      },
      type: {
        type: String
      },
      amount: {
        type: Number
      },
      currency: {
        type: String,
        value: "EUR"
      },
      category: {
        type: String
      },
      subcategory: {
        type: String
      },
      date: {
        type: Date
      },
      source: {
        type: String
      },
      subject: {
        type: String
      },
      detail: {
        type: String
      },
      address: {
        type: String
      },
      city: {
        type: String
      },
      community: {
        type: String
      },
      country: {
        type: String
      },
      accountId: {
        type: Number
      },
      loading:{
        type: Boolean,
        notify: true,
        value: false
      }
    };
  }
  reset(){
    this.amount = null;
    this.subject = null;
    this.$.radioGroupTipoMovimiento.selected = "";
  }

  _handleCancelarClick(evt){
    this.dispatchEvent(new CustomEvent('movement-form-cancel',{bubbles:true, composed:true}));
  }

  _handleAceptarClick(evt){
    this.loading = true;
    const normalizedMovement = {
      type: this.type,
      amount: parseInt(this.amount),
      currency: this.currency,
      category: this.category,
      subcategory: this.subcategory,
      date: this.date,
      source: this.source,
      subject: this.subject,
      detail: this.detail,
      address: this.address,
      city: this.city,
      community: this.comunity,
      country: this.country,
      account_id: this.accountId
    };
    this.$.addmovementdp.headers = {
      authorization : "JWT " + sessionStorage.getItem("token"),
      user_id : parseInt(sessionStorage.getItem("userId"))
    };
    this.$.addmovementdp.body = normalizedMovement;
    this.$.addmovementdp.host = cells.urlEasyMoneyBankBankServices;
    this.$.addmovementdp.generateRequest();
  }

  _handleRequestAddMovementSuccess(evt) {
    const detail = evt.detail;
    this.loading = false;
    let msg;
    let accountBalance;
    if (detail) {
      delete detail.headers;
      msg = detail.msg;
      accountBalance = detail.accountBalance;
    }
    this.dispatchEvent(new CustomEvent('add-movement-ok', {
      bubbles:true,
      composed:true,
      detail: {
            msg: msg,
            accountBalance: accountBalance
      }
    }));
  }

  _handleRequestAddMovementError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('add-movement-error', {
      bubbles: true,
      composed: true,
      detail: {
        msg: msg
      }
    }));
  }

  _handleCurrencyGroupSelectedChange(evt){
    const detail = evt.detail;
    delete detail.headers;
    this.currency = (detail.value == 0)? "EUR" : "USD";
  }
  
  _handleTipoMovimientoGroupSelectedChange(evt){
    const detail = evt.detail;
    delete detail.headers;
    this.type = (detail.value == 0)? "Ingreso" : "Reintegro";
  }

  _handleDateChanged(evt){
    const detail = evt.detail;
    if (detail) {
      delete detail.headers;
      this.date = detail.split('-').reverse().join('/');
    }
  }
}

customElements.define(EasyMoneyMovementForm.is, EasyMoneyMovementForm);